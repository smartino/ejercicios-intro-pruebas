import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class FindLastTest
{
   // this test fails!
   @Test public void lastOccurrenceInFirstElement() 
   {
      int arr[] = {2, 3, 5};
      int y = 2;
      assertEquals("Last occurence in first element", 0, FindLast.findLast(arr, y));
   }
   
   @Test public void EmptyArray() 
   {
      int arr[] = {};
      int y = 2;
      assertEquals("Empty Array", -1, FindLast.findLast(arr, y));
   }
   
   @Test public void lastOccurrenceInNotFirstElement() 
   {
      int arr[] = {2, 3, 5};
      int y = 5;
      assertEquals("Last occurence in last element", 2, FindLast.findLast(arr, y));
   }
   
   @Test public void NotInArray() 
   {
      int arr[] = {2, 3, 5};
      int y = 7;
      assertEquals("The integer is not in the array", -1, FindLast.findLast(arr, y));
   }
   


}

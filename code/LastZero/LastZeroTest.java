import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class LastZeroTest
{
   // this test fails!
   @Test public void multipleZeroes() 
   {
      int arr[] = {0, 1, 0};
      assertEquals("Multiple zeroes: should find last one", 2, LastZero.lastZero(arr));
   }
   
   @Test public void withoutZeroes() 
   {
      int arr[] = {3, 1, 2};
      assertEquals("Withou zeros", -1, LastZero.lastZero(arr));
   }
  
   
   @Test public void onlyOneZeroes() 
   {
      int arr[] = {3, 0, 2};
      assertEquals("One zero only", 1, LastZero.lastZero(arr));
   }
}
